// I am mainly using jQuery, here.

// When the DOM is ready, this function will be called
$(document).ready(function() {

    // Calling the random shuffler algorithm
    $('#randomAlgForm').on('submit', function(event) {
        // Prevent the default behavior of submit in the browser (reloading the page)
        event.preventDefault();
        var startTime = new Date().getTime(),
            firstNum = +$('input[name="firstNum"]').val(), // Send the values as an integer not String
            lastNum  = +$('input[name="lastNum"]').val(),
            range = Math.floor(lastNum - firstNum) + 1,
            rangeArray,
            processTime;

        // Check for bad input
        if (range <= 0) {
            $('#error-report').html("The first number should not be larger than the last number.").fadeIn();
            return;
        } else if (range > 900000 &&
                   !confirm("The range is large and it can cause the browser to be slow or even frozen for a few seconds. The time report would be still accurate, though. Are you willing to continue?")
        ) {
            return;
        }
        
        // Clean the previous error & time display if there is any
        $('#error-report').hide();
        $('.time').hide().html('');

        // Sending an array as a "reference" to the function to modify the array in place
        // and avoid copying large objects on return
        rangeArray = new Array(range);
        // Initializing the range array
        for(var i = 0; i < range; i++) {
            rangeArray[i] = i + firstNum;
        }

        // Record time
        displayTime(new Date().getTime() - startTime, 'Setup Time');

        // Shuffling
        processTime = range_shuffler(rangeArray);

        // Record time
        displayTime(processTime, 'Shuffling Time');
        // Display the results on the screen
        // NOTE: After profiling using firebug, we realized that for very large ranges,
        //       putting the entire array value into the textarea takes a few seconds and may cause the browser to 
        $('#results-nums').val(rangeArray.toString());
    });

    // Validating the 
    $.validate({
        modules : 'sanitize', // Needed for trimming the input
        validateOnBlur: true
    });

    // Change the pages with jQeury
    $('.link').on('click', function(event) {
        // Prevent the default behavior of a link in the browser (reloading the page)
        event.preventDefault();
        $('.content').slideToggle();
        $('.documentation').toggle();
        $('.algorithm').toggle();
        $('article').slideToggle();
    });
});

// The function will generate a random list of numbers between m and n exclusive.
// In other words, it is getting a range from [firstNum, lastNum], and shuffles them.
function range_shuffler(rangeArray) {
    // Start time of the algorithm (Need for speed evaluation)
    var startTime = new Date().getTime(),
        endTime,
        length = rangeArray.length;

    // shuffling the indexes uniformly and swap the array elements
    for (var i = 0; i < (length - 1); i++) {
        var rIdx = i + Math.floor(Math.random() * (length - i));
        //to avoid the overhead of a function call, we write swap function, here
        var temp = rangeArray[i];
        rangeArray[i] = rangeArray[rIdx];
        rangeArray[rIdx] = temp;
    }

    // Calculate the last end time of the algorithm 
    endTime = new Date().getTime();
    return (endTime - startTime);
}

// Display the message in
function displayTime(processTime, name) {
    // Using document create element as it is faster compare to jQuery one
    var liElement = $(document.createElement('li'));
    liElement.text(name + ': ' + processTime + ' millisecond' + ((processTime != 1)?'s':''));
    $('.time').append(liElement).fadeIn('slow');
}
